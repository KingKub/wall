#!/bin/bash
 while IFS=, read d l
 do
  
   noprefix=${l#*//};
   domain=${noprefix%%/*};

   if ping -w 5 -c 1 $domain > /dev/null; then
     echo trying $l 
     localUrl=${l//"http://"/"http://localhost/"}
     echo Showing $localUrl for  $d 
     #midori --display=:0  $l &
   else
     l=${l//"http://"/"http://localhost"}
   fi
   sleep 2s
 done < /home/pi/scripts/rotation.txt

# fi
