#!/bin/bash
## Laedt die Seiten herrunter. Passt dabei die Referenzen innerhalb der Seite an.

## Wir speichern und unserer screen.css. Die vom Server passt uns nicht.
 sudo cp /var/www/global/layout/2014/css/screen.css /home/pi/screen.css.bak
 ## Einmal alles aufraeumen (damit nichts ueber bleibt)
 sudo rm -rf /var/www/*
 ## Über alle Seiten der Rotation drueber gehen.
 while IFS=, read d l
 do
   page=${l//[$'\t\r\n']}
   echo Downloading $page >> /home/pi/scripts/log.txt
   noprefix=${page#*//}; 
   nosuffix=${noprefix%%/*};
   sudo wget  -nH --page-requisites --html-extension -P /var/www/ --domains langenau.dlrg.de,static.dlrg.de,$nosuffix -N -H $page   
 done < /home/pi/scripts/rotation.txt

 ## Wir laden noch eine gepatchte screen.css vom Server (damit wir die auch online anpassen koennten)
 sudo wget -N -P /home/pi/ https://langenau.dlrg.de/fileadmin/groups/14030010/screen.css
 sudo cp /home/pi/screen.css /var/www/global/layout/2014/css/screen.css
 ## Die Referenzen innerhalb der html Dateien passen nicht immer (bloedes https)
 find /var/www/ -name *.html -exec /home/pi/scripts/adjustBase.sh {} \;
