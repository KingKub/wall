#!/bin/bash

## Prueft ob ein reboot gewuenscht wird.
echo Checking Reboot >> /home/pi/scripts/log.txt


wget  http://langenau.dlrg.de/fileadmin/groups/14030010/reboot.txt >> /home/pi/scripts/log.txt
## Wenn das reboot.txt existiert starten wir neu.
if [ -a /home/pi/reboot.txt ] ; then
  echo rebooting wegen Rebootkommando >> /home/pi/scripts/log2.txt
  
  rm reboot.txt
  sudo rm -rf /tmp/*
  sudo reboot

fi
