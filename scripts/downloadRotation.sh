#!/bin/bash

## Laedt ein ggf. neues Rotation-File herunter und stoeßt dann den Download der neuen Seiten an.
echo checking for new file >> /home/pi/scripts/log.txt
cd /home/pi/scripts/
rm /home/pi/scripts/download_log.txt
wget -N http://langenau.dlrg.de/fileadmin/groups/14030010/rotation.txt -a /home/pi/scripts/download_log.txt

if ping -w 5 -c 1 langenau.dlrg.de > /dev/null; then
  if grep -c "Datei der Gegenseite ist neuer, erneuter Download" /home/pi/scripts/download_log.txt; then
        echo Downloading new Pages >> /home/pi/scripts/log.txt
        /home/pi/scripts/downloadPages.sh
	echo rebooting wegen neuer rotation  >> /home/pi/scripts/log.txt
	sudo rm -rf /tmp/*
	sudo reboot
  fi
fi
