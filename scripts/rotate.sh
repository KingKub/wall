#!/bin/bash
## Laesst die Seiten in der rotation.txt angegebenen Datei der Reihe nach im Midori anzeigen.
## Nimmt einen Screenshot nach der Anzeige

export DISPLAY=:0
echo Starting roation > /home/pi/scripts/log.txt
while [ TRUE ]; do

 while IFS=, read d l
 do
     ## Wir machen Midori. Damit wir kein Speicherproblem bekommen...
	 killall midori
	 
     page=${l//[$'\t\r\n']}
     noprefix=${page#*//};
     domain=${noprefix%%/*};
     
	 ## Ein bischen Vodoo um die URL loszuwerden
     localUrl=${page//"https://langenau.dlrg.de/"/"http://localhost/"}
     echo Showing loc $localUrl for  $d >> /home/pi/scripts/log.txt
     midori -e Fullscreen --display=:0 -a $localUrl &
	 ## Wir warten bis die Zeit abgelaufen ist
     sleep $d
	 
	 ## Wir machen einen Screenshot
     DISPLAY=":0.0"
     export DISPLAY
     fileName=/home/pi/screenshots/$(basename ${page::-1}).png
     echo Dumping to ${fileName} >> /home/pi/scripts/log.txt
     import -window root $fileName
 done < /home/pi/scripts/rotation.txt
 
done
