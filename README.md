# Projekt Beschreibung

Dieses Projekt sammelt einig Scripte mit deren Hilfe ein Digitaler Schaukasten umgesetzt wurde. Um die Kosten für unsere DLRG Ortsgruppe minimal zu halten wurde eine eine Rapsberry PI 2B und ein LG Monitor verwendet. Die geringe Rechenleistung und Speichergröße machte den einen oder anderen Kompromiss nötig.

Der Monitor untersützt die Darstellung in HD Qualität. Um eine Lesbarkeit der Inhalte über eine gewissen Distanz (Vorgabe 2-3m) zu erreichen musste das CSS angepasst werden. Seit die DLRG Seite nur noch via https ausgliefert wird
ist der geplante Man-In-The-Middle Angriff um das CSS auszutauschen/anzupassen gescheitert. Deshalb werden die Seiten via wget heruntergeladen und über einen localen apache ausgeliefert. Damit besteht auch die Möglichkeit das CSS auszutauschen.

# Eingesetzte Technologien und Hardware

- [RaspberryPi](https://www.raspberrypi.org/products/raspberry-pi-2-model-b/)
- LG Signage Monitor (kein Empfänger; für den 24/7 Betrieb ausgelegt)
- Linux ([Raspbian](https://www.raspberrypi.org/downloads/raspbian/))
- [Apache Webserver](https://httpd.apache.org/)
- [Midori](http://midori-browser.org/)

# Status des Projekts

Monitor und RaspberryPi wurden am Donnerstag 9.11.2017 im Langenauer nauBad aufgehängt und arbeitet dort z.Zt. produktiv.

![](doc/images/betrieb.jpg)

## Todos

- Scripte sollten auf Umgebungsparameter bassieren (für URL-Base https://langenau.dlrg.de/ und File-Base https://langenau.dlrg.de/fileadmin/groups/14030010/) 
- Rechte sollten nicht so häufig mit sudo ausgehebelt werden. Bisher wird das sehr freigiebig gehandhabt.
- Das CSS unterstütz noch nicht alle Features der Webseite. Eigentlich sind die Änderungen sogar nur sehr rudimentär. Bilder werden zum Beispiel noch zu klein dargestellt. Aber das wird durch Typo3 so gemacht. (Lösung siehe "zukukünfige Pläne")

# Zukünfigte Pläne

Der Download der Seiten und das Anpassen des CSS ist sehr fehleranfällig und sollte umgangen werden. Um dies zu erreichen könnte man spezielle Seitentemplates in das CMS auf www.dlrg.de einpflegen, die auch das Weiterschalten zwischen den Seiten übernehmen würden. So dass in Zukunft nur noch ein Standartbrowser mit einer Startseite geöffnet werden muss und das Weiterschalten vom Browser erledigt wird.

Außerdem wäre es sinnvoll, dass Seiten die nicht komplett auf die Seite passen automatisch hoch und runterscrollen (in Lesegeschwindigkeit).

 