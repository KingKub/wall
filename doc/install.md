# Installation

Folgende Elemente müssen installiert werden:

- Standart RaspberryPi 
- Midori
- Die Skripte
- Die Cron Einträge welche die Hilfskripte steuern
- Den Daemon der das Hauptskript ausführt


## Standard Pi Installation

Die normale Installation von **Raspbian** ist sehr einfach. 

Die Software lässt sich [hier](https://www.raspberrypi.org/downloads/) herunterladen. Eine detaillierte [Anleitung](https://www.raspberrypi.org/documentation/installation/installing-images/README.md) ist ebenfalls erhältlich.

Nach der Installation sollte zuerst eine Internet Verbindung hergestellt werden und die Software Pakete auf den neuesten Stand gebracht werden. Dazu einfach 

`sudo apt-get update
 sudo apt-get upgrade`

aufrufen    

## Midori Installation

Für Midori ist ein Debain Packet verfügbar, was seine Installation sehr vereinfacht:

`sudo apt-get install midori` 

Dieser Befehl sollte Midori und die benötigten Abhängigkeiten installieren.

## Die Skripte 

Die Skripte sind Teil des Projekts und können unter /scripts heruntergeladen werden.

Diese sollten im Augenblick in ein Verzeichnis /home/pi/scripts/ gelegt werden. Mit dem Befehl 

`sudo chmod a+x /home/pi/scripts/*.sh` 

werden die Skripte als ausführbar markiert. 

## Die Cronjobs

Mit `crontab -e` können die Cron Jobs verwaltet werden. In dieser Datei sollten folgende Zeilen eingefügt werden: 

`*/5 * * * * /home/pi/scripts/reboot.sh
 1 * * * * /home/pi/scripts/downloadRotation.sh
 30 23 * * * /home/pi/scripts/downloadPages.sh`

## Autostart des Hauptskrips

https://raspberry.tips/raspberrypi-einsteiger/raspberry-pi-autostart-von-skripten-und-programmen-einrichten/#LXDE_Autostarts

Vielleicht besser mit 

https://raspberry.tips/raspberrypi-einsteiger/raspberry-pi-autostart-von-skripten-und-programmen-einrichten/#Cronjob_fuumlr_Autostarts